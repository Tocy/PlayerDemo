/*
* Copyright (c) 2017 Zhangyujie (Tocy)

* This file is part of PlayerDemo project.
*/

#ifndef RENDER_YUV_HEADER_FILE_INCLUDED_20170224
#define RENDER_YUV_HEADER_FILE_INCLUDED_20170224

#include "SDL.h"

class YuvRenderer
{
public:
    YuvRenderer();
    ~YuvRenderer();

    bool Init(SDL_Rect show_rect);
    void Uninit();

    void Update(int width, int height, unsigned char * data[3], int stride[3]);
    bool Render();

private:
    SDL_Window * m_sdl_window;
    SDL_Renderer * m_sdl_renderer;

    // texture size
    int m_in_width, m_in_height;
    SDL_Texture * m_show_texture;

    SDL_Rect m_show_rect;

private:
    bool CreateTexture(int width, int height);
    void FillTexture(unsigned char *data[3], int stride[3]);
};

#endif