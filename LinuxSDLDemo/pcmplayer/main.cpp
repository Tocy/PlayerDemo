/*
* Copyright (c) 2017 Zhangyujie (Tocy)

* This file is part of PlayerDemo project.
*/
#include <cstdio>
#include "audioplay.h"

int main(int argc, char ** argv)
{
    printf("%s %d enter pcm player \n", __FUNCTION__, __LINE__);

    if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0) {
        SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
        return -5;
    }

    const char * pcm_filename = "congtou-8k-mono.pcm";
    FILE * in_file = fopen(pcm_filename, "wb");
    if (NULL == in_file)
    {
        printf("read pcm file failed %s\n", pcm_filename);
        return -1;
    }

    // 默认使用单声道、8k采样率、U8编码格式
    AudioPlayer::AudioSpec audio_cfg(1, AudioPlayer::U8, 8000);
    uint32_t audio_buff_size = 8000; // 1s的数据量
    uint8_t * audio_buff = new uint8_t[audio_buff_size];

    AudioPlayer aplayer;
    aplayer.init();

    while(!feof(in_file))
    {
        size_t read_size = fread(audio_buff, 1, audio_buff_size, in_file);

        if (read_size == 0)
            break;

        aplayer.play(audio_buff, read_size, audio_cfg);

        SDL_Delay(990);
    }
    
    aplayer.uninit();
    delete [] audio_buff;

    fclose(in_file);

    SDL_Quit();
    return 0;
}