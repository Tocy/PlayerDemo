/*
* Copyright (c) 2017 Zhangyujie (Tocy)

* This file is part of PlayerDemo project.
*/


#ifndef PLAYER_DEMO_DECODER_HEADER_FILE_20170117
#define PLAYER_DEMO_DECODER_HEADER_FILE_20170117

namespace PlayerDemo
{
    class Render;
    class Decoder 
    {
    public:
        Decoder();
        virtual ~Decoder();

        void setRender(Render &render);
        
        int init();
        int decodeFrame(AVPacket &packet);
        void uninit();
    };
}

#endif // end of #ifndef PLAYER_DEMO_DECODER_HEADER_FILE_20170117
