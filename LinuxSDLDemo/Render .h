/*
* Copyright (c) 2017 Zhangyujie (Tocy)

* This file is part of PlayerDemo project.
*/


#ifndef PLAYER_DEMO_RENDER_HEADER_FILE_20170117
#define PLAYER_DEMO_RENDER_HEADER_FILE_20170117

namespace PlayerDemo
{
    class Render
    {
    public:
        Render();
        virtual ~Render();

        int init(SDL_Rect pos);

        int display(AVFrame * frame);

        void uninit();
    };
}

#endif // end of #ifndef PLAYER_DEMO_RENDER_HEADER_FILE_20170117
