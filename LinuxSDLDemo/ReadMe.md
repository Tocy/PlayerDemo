Compile with Ubuntu 15.04 with:
+ ffmpeg-3.2.2.tar.bz2
./configure --prefix="./out/" --disable-debug --disable-encoders --disable-muxers --disable-devices --disable-filters --disable-ffplay --disable--ffprobe
+ SDL2-2.0.5

This is a demo player based on FFmpeg's ffplay & SDL.

just demux and decode video frame  using FFmpeg, then render it using SDL.